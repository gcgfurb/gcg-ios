//
//  main.m
//  iOS-N3_Mundo
//
//  Created by LabCG on 17/10/13.
//  Copyright (c) 2013 Dalton Reis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Mundo.h"
#import "Poligono4D.h"
#import "Circulo.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSLog(@"-- iOS-N3_Mundo --");
        Mundo *mundo = [[Mundo alloc]init];
        
        Poligono4D *objeto_1 = [[Poligono4D alloc]init];
        [objeto_1 AdicionarPonto4D:10 :20 :30];
        [objeto_1 AdicionarPonto4D:40 :50 :60];
        [mundo AdicionarObjetoGrafico:objeto_1];
        
        Circulo *circulo_1 = [[Circulo alloc]init:50 :[[Ponto4D alloc]init:40 :50 :60]];
        [mundo AdicionarObjetoGrafico:circulo_1];
        
        [mundo DesenhaCena];
        
    }
    return 0;
}

