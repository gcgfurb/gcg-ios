//
//  main.m
//  iOS-N3_Point4D-Polygon4D
//
//  Created by LabCG on 15/10/13.
//  Copyright (c) 2013 Dalton Reis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Poligono4D.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSLog(@"-- iOS-N3_Point4D-Polygon4D --");
        Poligono4D *poligono = [[Poligono4D alloc]init];
        [poligono ListarPoligono];
        [poligono AdicionarPonto4D:20 :20 :20];
        [poligono AdicionarPonto4D:30 :30 :30];
        [poligono ListarPoligono];
        [poligono InserirPonto4D:0 :10 :10 :10];
        [poligono ListarPoligono];
        [poligono RemoverUltimoPonto4D];
        [poligono ListarPoligono];
        [poligono RemoverTodosPonto4D];
        [poligono ListarPoligono];
    }
    return 0;
}

