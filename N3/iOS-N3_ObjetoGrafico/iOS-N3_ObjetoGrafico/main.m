//
//  main.m
//  iOS-N3_ObjetoGrafico
//
//  Created by LabCG on 17/10/13.
//  Copyright (c) 2013 Dalton Reis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ObjetoGrafico.h"
#import "Poligono4D.h"
#import "Circulo.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSLog(@"-- iOS-N3_ObjetoGrafico --");
        Poligono4D *objeto_1 = [[Poligono4D alloc]init];
        [objeto_1 TrocaVisivel];
        if ([objeto_1 ehVisivel]) {
            NSLog(@"Objeto visivel");
        }
        [objeto_1 AdicionarPonto4D:10 :20 :30];
        [objeto_1 ListarPoligono];

        ObjetoGrafico *objeto_2 = [[Poligono4D alloc]init];
        if ([objeto_2 ehVisivel]) {
            NSLog(@"Objeto visivel");
        }
        if([objeto_2 isMemberOfClass:[Poligono4D class]]){
            Poligono4D *poligono = (Poligono4D *) objeto_2;
            [poligono AdicionarPonto4D:40 :50 :60];
            [poligono ListarPoligono];
        }
        
        Circulo *circulo_1 = [[Circulo alloc]init:50 :[[Ponto4D alloc]init:70 :80 :90]];
        if ([circulo_1 isMemberOfClass:[ObjetoGrafico class]]) {
            [circulo_1 ListarCirculo];
        }
        if ([circulo_1 isMemberOfClass:[Circulo class]]) {
            [circulo_1 ListarCirculo];
        }
    }
    return 0;
}
