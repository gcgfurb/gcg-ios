//
//  main.m
//  iOS-N3_Point4D-Polygon4D
//
//  Created by LabCG on 15/10/13.
//  Copyright (c) 2013 Dalton Reis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Ponto4D.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSLog(@"-- iOS-N3_Ponto4D --");
        Ponto4D *ptoA = [[Ponto4D alloc] init];
        Ponto4D *ptoB = [[Ponto4D alloc] init:10 :20 :30];
        NSLog(@"%@",[ptoA ListarPonto4D]);
        NSLog(@"%@",[ptoB ListarPonto4D]);
    }
    return 0;
}

