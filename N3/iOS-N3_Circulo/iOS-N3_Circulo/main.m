//
//  main.m
//  iOS-N3_Circulo
//
//  Created by LabCG on 17/10/13.
//  Copyright (c) 2013 Dalton Reis. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Circulo.h"
#import "Ponto4D.h"

int main(int argc, const char * argv[])
{

    @autoreleasepool {
        NSLog(@"-- iOS-N3_Circulo --");
        Ponto4D *ponto = [[Ponto4D alloc]init:10 :20 :30];
        Circulo *circulo_1 = [[Circulo alloc]init:50 :ponto];
        [circulo_1 ListarCirculo];
        Circulo *circulo_2 = [[Circulo alloc]init:100 :[[Ponto4D alloc]init:40 :50 :60]];
        [circulo_2 ListarCirculo];
    }
    return 0;
}

