//
//  Environment.h
//  Environment-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "ObjetoGrafico.h"

@interface Mundo : NSObject {
@private
	NSMutableArray *listaObjetoGrafico;
}

@property (readonly, getter = getObjetoGrafico) NSMutableArray *listaObjetoGrafico;

-(void) AdicionarObjetoGrafico:(ObjetoGrafico *)objetoGrafico;
-(void) DesenhaCena;

@end
