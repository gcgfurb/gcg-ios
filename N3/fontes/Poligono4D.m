//
//  Polygon4D.m
//  Polygon4D-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "Poligono4D.h"
#import "Ponto4D.h"

@implementation Poligono4D

-(id) init {
	self = [super init];
	if (self != nil) {
		listaPonto4D = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void)ListarPoligono {
    NSLog(@"--------------------------------");
    if ([listaPonto4D count] == 0)
        NSLog(@" ... lista vazia");
    else
        for (NSUInteger i = 0; i < [listaPonto4D count]; i++)
            NSLog(@"%@",[[listaPonto4D objectAtIndex:i] ListarPonto4D]);
}

-(void)AdicionarPonto4D:(double)x :(double)y :(double)z {
	Ponto4D *point4D;
	point4D = [[Ponto4D alloc] init:x :y :z];
	[listaPonto4D addObject:point4D];
}

-(void)InserirPonto4D:(unsigned)indice :(double)x :(double)y :(double)z {
	Ponto4D *point4D;
	point4D = [[Ponto4D alloc] init:x :y :z];
    [listaPonto4D insertObject:point4D atIndex:indice];
}

-(void)RemoverUltimoPonto4D {
    [listaPonto4D removeLastObject];
}

-(void)RemoverTodosPonto4D {
    [listaPonto4D removeAllObjects];
}

-(void)Desenha {
    NSLog(@"%@ - Desenha", [self description]);
}

@end
