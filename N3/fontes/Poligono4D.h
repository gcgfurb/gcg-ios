//
//  Polygon4D.h
//  Polygon4D-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "ObjetoGrafico.h"

/**
 @brief Classe que define Poligono4D
 */
@interface Poligono4D : ObjetoGrafico {
    @private
    /**
     @brief lista de Ponto4D que representam os vértices do póligono.
     */
	NSMutableArray *listaPonto4D;
}

/**
 @return Retorna lista de Ponto4D (vértices do póligono).
 */
-(void)ListarPoligono;

/**
 @brief Adiciona Ponto4D na lista.
 @param valor de x
 @param valor de y
 @param valor de z
 */
-(void)AdicionarPonto4D:(double)x :(double)y :(double)z;

/**
 @brief Adiciona Ponto4D na lista na posição do índice.
 @param valor do índice
 @param valor de x
 @param valor de y
 @param valor de z
 */
-(void)InserirPonto4D:(unsigned)indice :(double)x :(double)y :(double)z;

/**
 @brief Remove o último Ponto4D da lista.
 */
-(void)RemoverUltimoPonto4D;

/**
 @brief Remove TODOS os Ponto4D da lista.
 */
-(void)RemoverTodosPonto4D;

/**
 @brief Desenha o poligono usando a lista de pontos (vertices)
 */
-(void)Desenha;

@end
