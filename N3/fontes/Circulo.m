//
//  Circle.m
//  Circle-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "Circulo.h"

@implementation Circulo

@synthesize raio;
@synthesize centro;

-(id)init:(double)_raio :(Ponto4D*)_centro {
	self = [super init];
	if (self != nil) {
        raio = _raio;
        centro = _centro;
	}
	return self;
}

-(void)ListarCirculo {
    NSLog(@"%@ - Raio: %f - %@", [self description], [self getRaio], [[self centro] ListarPonto4D]);
}

-(void)Desenha {
    NSLog(@"%@ - Desenha", [self description]);
}

@end
