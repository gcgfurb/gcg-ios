//
//  Circle.h
//  Circle-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "ObjetoGrafico.h"
#import "Ponto4D.h"

@interface Circulo : ObjetoGrafico {
@private
	double raio;
	Ponto4D *centro;
}

@property (getter = getRaio, setter = setRaio:) double raio;
@property (readonly) Ponto4D *centro;

-(id)init:(double)_raio :(Ponto4D*)_centro;
-(void)ListarCirculo;
-(void)Desenha;


@end
