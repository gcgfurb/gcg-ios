//
//  Environment.m
//  Environment-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "Mundo.h"
#import "Poligono4D.h"
#import "Circulo.h"

@implementation Mundo

@synthesize listaObjetoGrafico;

-(id) init {
	self = [super init];
	if (self != nil) {
		listaObjetoGrafico = [[NSMutableArray alloc] init];
	}
	return self;
}

-(void) AdicionarObjetoGrafico:(ObjetoGrafico *)objetoGrafico {
	[listaObjetoGrafico addObject:objetoGrafico];
}

-(void) DesenhaCena {
    for (NSUInteger i = 0; i < [listaObjetoGrafico count]; i++) {
        ObjetoGrafico *objetoGrafico = [listaObjetoGrafico objectAtIndex:i];
        if ([objetoGrafico isMemberOfClass:[Poligono4D class]]) {
            Poligono4D *poligono = (Poligono4D *) objetoGrafico;
            [poligono Desenha];
        }
        else if ([objetoGrafico isMemberOfClass:[Circulo class]]) {
            Circulo *circulo = (Circulo *) objetoGrafico;
            [circulo Desenha];
        }
        else
            NSLog(@"%@ - Objeto nao pode ser desenhado", [self description]);            
    }
}

@end
