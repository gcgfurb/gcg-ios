//
//  GraphicObj.h
//  GraphicObj-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ObjetoGrafico : NSObject {
@private
	BOOL visivel;
}

@property (readonly, getter=ehVisivel) BOOL visivel;

-(void) TrocaVisivel;

@end
