//
//  Point4D.m
//  Point4D-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "Ponto4D.h"

@implementation Ponto4D

@synthesize x;
@synthesize y;
@synthesize z;

-(id) init {
	self = [super init];
	if (self != nil) {
		x = 0;
		y = 0;
		z = 0;
		w = 1;
	}
	return self;
}

-(id)init:(double)_x :(double)_y :(double)_z {
	self = [super init];
	if (self != nil) {
		x = _x;
		y = _y;
		z = _z;
		w = 1;
	}
	return self;
}

-(NSString*) ListarPonto4D {
	return([NSString stringWithFormat: @"%@ - Pto: %f %f %f", [self description], [self getX],[self getY],[self getZ]]);
}

@end
