//
//  Point4D.h
//  Point4D-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 @brief Classe que define Ponto4D
 */
@interface Ponto4D : NSObject {
    @private
    /**
     @brief valor da coordenada X.
     */
    double x;
    /**
     @brief valor da coordenada y.
     */
    double y;
    /**
     @brief valor da coordenada z.
     */
    double z;
    /**
     @brief valor da coordenada w (espaço homogenêo).
     */
    double w;
}

/**
 @return Retorna o valor da coordenada x.
 */
@property (getter = getX, setter = setX:) double x;

/**
 @return Retorna o valor da coordenada y.
 */
@property (getter = getY, setter = setY:) double y;

/**
 @return Retorna o valor da coordenada z.
 */
@property (getter = getZ, setter = setZ:) double z;

/**
 @brief Inicializado valores do ponto.
 @param valor de x
 @param valor de y
 @param valor de z
 */
-(id)init:(double)x :(double)y :(double)z;


/**
 @brief Converte valores do ponto em texto.
 @param String com valores x,y e z do ponto
 */
-(NSString*) ListarPonto4D;

@end
