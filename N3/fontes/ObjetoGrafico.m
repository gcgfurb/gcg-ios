//
//  GraphicObj.m
//  GraphicObj-class
//
//  Created by Dalton Reis on 22/04/10.
//  Copyright 2010 FURB. All rights reserved.
//

#import "ObjetoGrafico.h"


@implementation ObjetoGrafico

@synthesize visivel;

-(id) init {
	self = [super init];
	if (self != nil) {
		visivel = YES;
	}
	return self;
}

-(void) TrocaVisivel {
    visivel = !visivel;
}

@end
